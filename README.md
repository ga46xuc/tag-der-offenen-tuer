Tag der offenen Tür -- Physik-Department
========================================

Organisation für den Tag der offenen Tür am Physik-Department

Der nächste Tag der offnen Tür findet statt am: 
   **Samstag, den 13. Oktober 2018, von 11 bis 18 Uhr**

**Achtung: Nach dem Editieren unten auf das grüne "Commit changes" drücken, um die Datei zu speichern!!!**
Ohne "Commit" gehen die Änderungen verloren! 

Aktueller Stand
---------------

- Vortragsprogramm: [tdot-vortraege.txt](tdot-vortraege.txt)
- Foyer / Laborbesichtigungen: [tdot-foyer.include](tdot-foyer.include)

Zum Editieren einfach 

1. jeweiligen Dateinamen anklicken 
2. oben auf "edit" gehen (normales TUM-, LRZ-, LMU-Login)
3. editieren
4. unten auf "Commit changes" drücken. 

**Selbstverständlich können Sie mir Ihren Vortrag / Beitrag auch gerne mailen!**

Mail an: [outreach@ph.tum.de](mailto:outreach@ph.tum.de)

Das fertige Programm findet sich (nach Freigabe!) unter: https://www.ph.tum.de/tdot/

Fragen und Anregungen bitte an Johannes Wiedersich / [outreach@ph.tum.de](mailto:outreach@ph.tum.de) .

Vergangene Termine
------------------

- Samstag, der 21. Oktober 2017, 11 bis 18 Uhr
- Samstag, der 22. Oktober 2016, 11 bis 18 Uhr

